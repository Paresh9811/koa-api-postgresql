const Router = require('koa-router')
const router = new Router()
const AuthorController = require('./author/controller')
const BookController = require('./books/controller')

// Author API END Points
router.post ('/authors' , AuthorController.createAuthor)
router.get ('/authors' , AuthorController.getAllAuthor )
router.get('/authors/:id' , AuthorController.getAuthorById) 
router.delete('/authors/:id' , AuthorController.deleteAuthorById)
router.patch('/authors/:id' , AuthorController.updateAuthorById)

// Book API END Points
router.post('/books/author-id/:authorId' , BookController.createBook)
router.get('/books' , BookController.getAllBooks)
router.get('/books/:id' , BookController.getBookById)
router.delete('/books/:id' , BookController.deleteBookById)
router.patch('/books/:id' , BookController.updateBookById)

// Exporting Router.
module.exports ={router}