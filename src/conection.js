require('dotenv').config()
const postgres = require('postgres')

const sql = postgres({
	user		: process.env.USER,
	host		: process.env.HOST,
	database	: process.env.DATABASE,
	password	: process.env.PASSWORD,
	port		: process.env.DB_PORT
})

module.exports = sql