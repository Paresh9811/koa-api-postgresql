const sql = require('../conection')

// POST Author Model
const createAuthorModel = async (fields) => {

	const author = await sql `INSERT INTO author (name , email, city ) 
	VALUES (${fields.name} , ${fields.email} ,${fields.city} )
	RETURNING *`

	return author
}

// Get All Author Model. 
const getAllAuthorModel = async () => {
	
	const authors = await sql `SELECT * FROM author `
	return  authors
}

// Get author By ID Author Model.
const getAuthorByIdModel = async(id) => {
	
	const author = await sql `SELECT * FROM author WHERE id = ${id}`
	return author
}

// DELETE author By ID Author Model..
const deleteAuthorByIdModel = async(id) => {
	
	const author = await sql `DELETE FROM author WHERE id = ${id}` 
	return author
}

// UPDATE author by ID Author Model .
const updateAuthorByIdModel = async (fields,id) => {
	
	const author = await sql `UPDATE author 
	SET name = ${fields.name} ,
	email = ${fields.email} , 
	city = ${fields.city} 
	WHERE id = ${id} RETURNING *`
	return author 
}

// Exporting All Model ....
module.exports = {
	createAuthorModel, 
	getAllAuthorModel,
	getAuthorByIdModel,
	deleteAuthorByIdModel,
	updateAuthorByIdModel
}