const Joi = require('joi')
const AuthorModel = require('./model')

//POST author Controller.
const createAuthor = async (ctx) => {

	const schema = Joi.object({
		name  : Joi.string().min(3).max(15).required(),
		email : Joi.string().min(3).max(100).email().required(),
		city  : Joi.string().min(3).max(15).required(),
	})
	const {value:field , error:error} = schema.validate(ctx.request.body)

	if(!error){
		const author = await AuthorModel.createAuthorModel(field)
		ctx.body     = author
		ctx.status	 = 200
	}else {
		ctx.body = {
			error : {
				message: error.details[0].message
			}
		}
		ctx.status = 400
	}
}

// GET All Author Controller
const getAllAuthor = async(ctx) =>{

	const author = await AuthorModel.getAllAuthorModel(ctx)

		if(author.count !== 1) {
			ctx.body 	= author
			ctx.status	= 200
		}else{
			ctx.body = {
				error: {
					message : `No data available in Author..`
				}
			}
			ctx.status = 404
		}
}

// GET Author BY ID Controller..
const getAuthorById = async (ctx) => {

	const id	 = ctx.params.id
	const author = await AuthorModel.getAuthorByIdModel(id)

	if(author.count == 1){
		ctx.body   = author
		ctx.status = 200
	}else{
		ctx.body = {
			error :{
				message : `Requsted ID is not found`
			}
		}
		ctx.status = 404
	}
}

//DELETE author by ID Controller..
const deleteAuthorById = async (ctx) =>{

	const id	 = ctx.params.id
	const author = await AuthorModel.deleteAuthorByIdModel(id)
	
	if(author.count == 1){
		ctx.body = {
			success : {
				message : `Requested id Is succesfully Deleted...` 
			} 
		}
		ctx.status = 200
	}else{
		ctx.body = {
			error : {
				message : `Requested Id is NOt Found in database..`
			}
		}
		ctx.status = 404
	}
} 

// UPDATE author By ID Controller.
const updateAuthorById = async (ctx) => {

	const schema = Joi.object({
		name  : Joi.string().min(3).max(15).required(),
		email : Joi.string().min(3).max(100).email().required(),
		city  : Joi.string().min(3).max(15).required(),
	})

	const {value:fields , error :error}	= schema.validate(ctx.request.body)
	
	const id     = ctx.params.id
	const author = await AuthorModel.updateAuthorByIdModel(fields , id)

		if(author.count == 1){
			if(!error){
				ctx.body = {
						success : {
							UpdatedAuthor:author
						} 
					}
				ctx.status = 200
			}else{
				ctx.body = {
						error : {
							message : error.details[0].message
						}
					}
				ctx.status = 400
			}
		}else{
			ctx.body = {
				error : {
					message : `Requsted ID is not Fond...`
				}
			}
		}
}
		
// exporting Controllers.
module.exports = {
	createAuthor,
	getAllAuthor,
	getAuthorById,
	deleteAuthorById,
	updateAuthorById,	
}