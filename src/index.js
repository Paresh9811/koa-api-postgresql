const Koa = require('koa')
const bodyParser = require('koa-bodyparser')
require('dotenv').config()
const app = new Koa()
app.use(bodyParser())
const routes = require('./routes')
const router = routes.router

// Add routes and response to the OPTIONS requests
app.use(router.routes()).use(router.allowedMethods())

const port = process.env.PORT
app.listen(port, () => console.log(`litsenig port ${port}...`))