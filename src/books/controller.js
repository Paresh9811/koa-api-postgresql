const Joi = require('joi')
const BooksModel = require('./model')

// POST Book
const createBook = async(ctx) => {

	const schema = Joi.object({
		name: Joi.string().min(3).required(),
	})

	const {value: field ,error : error} = schema.validate(ctx.request.body) 
	const id = ctx.params.authorId

		if(!error){
			const book = await BooksModel.createBookDataModel(field , id)

				if(book.count == 1){
					ctx.body = book
					ctx.status = 200
				}else{
					ctx.body = {
						error : {
							message : `Given Author id is not valid Author ID`
						}
					}
				}
		}else{
			ctx.body = {
				error : {
					message : error.details[0].message
				}
			}
			ctx.status = 400
		}
}

// GET All Books
const getAllBooks =  async(ctx) =>{

	const books = await BooksModel.getAllBooksModel()

	if(books.count !== 1){
		ctx.body   = books
		ctx.status = 200
	}else{
		ctx.body = {
			error : {
				message : `No Data Available in books `
			}
		}
		ctx.status = 404
	}
}

// GET Book By ID.
const getBookById = async(ctx ) => {
	
	const bookId = ctx.params.id
	const book = await BooksModel.getBookByIdModel(bookId)
	
	if(book.count == 1) {
		ctx.body = book
		ctx.status = 200
	}else{
		ctx.body = {
			error : {
				message :`Requsted ID is not found`
			}
		}
		ctx.status = 400
	}		
}

// DELETE Book By ID.
const deleteBookById = async(ctx) => {
	
	const id = ctx.params.id
	const book = await BooksModel.deleteBookByIdModel(id)

	if(book.count == 1){
		ctx.body = {
			success : {
				message : `Requsted ID succesfully deleted..`
			}
		}
		ctx.status = 200
	}else{
		ctx.body = {
			error : {
				message : `Requsted ID is not found`
			}
		}
		ctx.status = 400
	}
}

// UPDATE Book By ID.
const updateBookById = async(ctx) => {
	
	const schema = Joi.object({
		name  : Joi.string().min(2).max(100).required()
	})

	const {value:fields , error:error} = schema.validate(ctx.request.body)

	const id = ctx.params.id
	const book = await BooksModel.updateBookByIdModel(fields , id)
	console.log(book)
	
	
		if(book.count == 1){
			if(!error){
				ctx.body = {
					success : {
						message : `Requsted ID Updated Succesfully..`
					}
				}
				ctx.status = 200
			}else{
				ctx.body = {
					error :{
						message : error.details[0].message
					}
				}
				ctx.status = 400
			}
		}else{
			ctx.body = {
				error : {
					message : `Requsted ID is not found`
				}
			}
			ctx.status = 404
		}
}

module.exports = {
	createBook,
	getAllBooks,
	getBookById,
	deleteBookById,
	updateBookById,	
}