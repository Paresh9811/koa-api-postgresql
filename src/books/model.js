const sql = require('../conection')

// CREATE Book Model
const createBookDataModel = async(field, id ) => {

	try{
		const [book] = await sql `
		INSERT INTO books (name , author_id)
		VALUES (${field.name} , ${id})
		RETURNING id`

		const bookDetails = await getBookByIdModel(book.id)		
							
		return bookDetails

	}catch(error){

		return  error.detail
	}
}

// GET All Books Model
const getAllBooksModel = async() => {

	const books = await sql `SELECT
	books.name as bookName , 
	author.name as authorName, 
	author.email as authorEmail ,
	author.city as AuthorCity 
	FROM author 
	JOIN books 
	ON (author.id = books.author_id)`

	return books
}

// GET Book By ID.
const getBookByIdModel = async (id) => {
	
	const book = await sql `SELECT
	books.name as bookname , 
	author.name as authorname,
	author.email as authoremail, 
	author.city as authorcity
	FROM books 
	JOIN author
	ON (author.id = books.author_id) 
	WHERE books.id = ${id}`
	
	return book  
}

// DELETE Book By ID.
const deleteBookByIdModel = async (id) => {

	const book = await sql `DELETE FROM books WHERE id = ${id}`

	return book  
}

// UPDATE Book By ID.
const updateBookByIdModel = async(fields, id) => {
	
	const book = await sql `UPDATE books 
	SET name = ${fields.name} ,
	author_Id = ${fields.author_id} 
	WHERE id = ${id} `
	
	return book
}

// Exporting Models
module.exports = {
	getAllBooksModel,
	getBookByIdModel,
	deleteBookByIdModel,
	updateBookByIdModel,
	createBookDataModel
}